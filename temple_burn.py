# Slow burning

import opc, time
from time import sleep
from utils import fade, interpolate
from random import randint, choice

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')

length=1000
flame_colors = [(100, 17, 0), (150, 34, 3), (215, 53, 2), (252, 100, 0), (255, 117, 0), (250, 192, 0), (238,153,17),  	(240,185,4),  	(107,35,4), (0, 0, 0), 	(210,111,4), (255,0,0)]
flames = [fade((0,0,0),choice(flame_colors),10,10) for x in range(length)]

pixels = [ (0,0,0) ] * length
while True:
        
    for i, f in enumerate(flames):
        if f.remaining == 0:
            f.last = f.next
            f.next = choice(flame_colors)
            f.steps = randint(5,50)
            f.remaining = f.steps
        else:
            f.remaining -= 1
        r = 1-(f.remaining / f.steps)
        c = interpolate(f.last, f.next, r)
        pixels[i] = (c[1],c[0],c[2])
    client.put_pixels(pixels)
    time.sleep(0.04)