Tour De Chance Python Lighting Scripts
========================


Simple LED Patterns
-------------------

* `chase.py`
  * Spontaneously generates rainbow-colored blobs at the start of the sign "TDC", and send them along the length
  * At the end, they wrap around and continue from the start.
  * Over time, the sign fills up with colored blobs flowing. Eventually resets back to black and starts again.
* `france_1.py`
  * Blue-White-Red patterns, which change how zoomed in they are.
  * Starts with one led per color, then two, then four, and so on, until each letter is a single color - then reverses.
* `france_2.py`
  * Lights each letter with a color of the French Flag, and adds a sparkle effect
* `squidsoup.py`
  * Shameless and poor-quality rip-off of my favorite effect from Squid Soup's Desert Wave (BM 2019)
* `unicorn_puke.py`
  * Rainbow LEDs - zoomed in, so every 7 LEDs constitues a whole spectrum. 
* `man_burn.py`
  * Flame effect which only comes on for Man-Burn night
  * (more like embers, since I don't know which way is "up" for each letter, so the fire can't really flow)
* `rainbow.py`
  * Standard rainbow rotating across the whole sign
* `temple_burn.py`
  * As with man_burn, but slower, to suit the more somber Temple Burn
* `neon.py`
  * Imitates an old neon orange sign
  * Occasionally flickers as if something's gone wrong, then does something
  * Something ranges from normal (spelling out Tour-De-Chance) to weird (spelling rude words)
* `sparkle.py`
  * Standard rainbow effect, but with sparkles
* `theme.py`
  * Lights each letter according to the color it's painted, (Green, Yellow, Polka)
  * Gentle moving effect


Services
--------

* start_led_program.sh
  * Takes a chosen program name, and copies the script to the "RUNNING.py" file
  * Also reads switch value, and may override chose program with burn night programs
  * Called by cron
  * Calls/restarts systemd

* crontab
  * Picks a new program to run at an appropriate interval
  * Triggers start_led_program
  * Runs through all programs every hour

* sign-leds.service
  * systemd service which keeps the "RUNNING.py" python script running
  * Restarted regularly by start_led_program.sh, which replaces the content of "RUNNING.py"