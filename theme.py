# Set each letter to the painted color

import opc, time
from time import sleep
from utils import fade, interpolate
from random import randint, choice

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')

numLEDs=1024

yellow = (255,255,0)
green = (0,255,0)
white = (255,255,255)
red = (255,0,0)
dots = [red, white]

colors = {
    'yellow': yellow,
    'green': green,
    'white': white,
    'red': red,
    'dots': dots,
}
order = ['yellow', 'green', 'dots', 'white'] * 3
# print(order)

pixels = [ (0,0,0) ] * numLEDs
r = 0
x = 1
while True:
    x = 1 if (x==0) else 0
    for i, letter in enumerate(order):
        # print(i, letter)
        if letter == 'dots':
            for j in range(0, 50):
                if int(j/2) % 2 == 0:
                    c = red
                else:
                    c = white
                # print(c)
                pixels[(i*50) + j] = (c[1] + randint(-r,r), c[0] + randint(-r,r), c[2] + randint(-r,r)) if j %2 == x else (0,0,0)
        if letter == 'yellow':
            for j in range(0, 50):
                c = yellow
                # print(c)
                pixels[(i*50) + j] = (c[1] + randint(-r,r), c[0] + randint(-r,r), c[2] + randint(-r,r)) if j %2 == x else (0,0,0)
        if letter == 'green':
            for j in range(0, 50):
                c = green
                # print(c)
                pixels[(i*50) + j] = (c[1] + randint(-r,r), c[0] + randint(-r,r), c[2] + randint(-r,r)) if j %2 == x else (0,0,0)
        if letter == 'white':
            for j in range(0, 50):
                c = white
                # print(c)
                pixels[(i*50) + j] = (c[1] + randint(-r,r), c[0] + randint(-r,r), c[2] + randint(-r,r)) if j %2 == x else (0,0,0)
    client.put_pixels(pixels)
    time.sleep(1)