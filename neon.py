# Neon style sign, but haunted

import opc
from time import sleep
# from utils import fade, interpolate
from random import randint, choice

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')

numLEDs=1024
leds_per_letter = 50
color = (95, 255, 31)  # neon orange

words = {
    #          0 1 2 3 4 5 6 7 8 9 A B
    #          t o u r d e c h a n c e
    'turd':   [1,0,1,1,1,0,0,0,0,0,0,0,],
    'touche': [1,1,1,0,0,0,1,1,0,0,0,1,],
    'toucan': [1,1,1,0,0,0,1,0,1,1,0,0,],
    'dance':  [0,0,0,0,1,0,0,0,1,1,1,1,],
    'race':   [0,0,0,1,0,0,0,0,1,0,1,1,],
    'ha':     [0,0,0,0,0,0,0,1,1,0,0,0,],
    'tour':   [1,1,1,1,0,0,0,0,0,0,0,0,],
    'de':     [0,0,0,0,1,1,0,0,0,0,0,0,],
    'the':    [1,0,0,0,0,0,0,1,0,0,0,1,],
    'chance': [0,0,0,0,0,0,1,1,1,1,1,1,],
    'ouch':   [0,1,1,0,0,0,1,1,0,0,0,0,],
    'eh':     [0,0,0,0,0,1,0,1,0,0,0,0,],
    'cha':    [0,0,0,0,0,0,1,1,1,0,0,0,],
    'on':     [1,1,1,1,1,1,1,1,1,1,1,1,],
    'off':    [0,0,0,0,0,0,0,0,0,0,0,0,],
    't':      [1,0,0,0,0,0,0,0,0,0,0,0,],
    'o':      [0,1,0,0,0,0,0,0,0,0,0,0,],
    'u':      [0,0,1,0,0,0,0,0,0,0,0,0,],
    'r':      [0,0,0,1,0,0,0,0,0,0,0,0,],
    'd':      [0,0,0,0,1,0,0,0,0,0,0,0,],
    'e':      [0,0,0,0,0,1,0,0,0,0,0,0,],
    'c':      [0,0,0,0,0,0,1,0,0,0,0,0,],
    'h':      [0,0,0,0,0,0,0,1,0,0,0,0,],
    'a':      [0,0,0,0,0,0,0,0,1,0,0,0,],
    'n':      [0,0,0,0,0,0,0,0,0,1,0,0,],
    'C':      [0,0,0,0,0,0,0,0,0,0,1,0,],
    'E':      [0,0,0,0,0,0,0,0,0,0,0,1,],

}

def show_word(word):
    pixels = [ (0,0,0) ] * numLEDs
    letters = words[word]
    for i, letter in enumerate(letters):
        if letter == 1:
            # print(list("tourdechance")[i], i)
            for x in range(leds_per_letter*i, leds_per_letter*(i+1)):
                pixels[x] = color
                # print(x)
        # print()
    client.put_pixels(pixels)
    sleep(0.01)
    client.put_pixels(pixels)
    return pixels

def all_on():
    return show_word('on')

def all_off():
    return show_word('off')


def flicker(r=4, word='on'):
    for i in range(r):
        show_word(word)
        sleep(float(randint(7,9)/1000.0))
        all_off()
        sleep(float(randint(7,9)/1000.0))


def wait():
    z = 5  # starting wait
    sleep(float(z/2.0))
    flicker(randint(1,7))
    all_on()
    sleep(z/2.0)
    

def ha():
    print('ha')
    all_on()
    wait()
    flicker(7)
    for i in range(4):
        all_off()
        sleep(0.6)
        show_word('ha')
        sleep(0.6)
    flicker()
    flicker()
    all_off()
    sleep(0.5)
    flicker(6)
    all_on()
    wait()

def turd():
    print('turd')
    all_on()
    wait()
    flicker(7)
    sleep(1)
    flicker(7)
    sleep(1)
    flicker(7)
    all_off()
    sleep(1)
    show_word('turd')
    sleep(1)
    flicker(7, 'turd')
    show_word('turd')
    sleep(5)
    flicker(7, 'turd')
    flicker(7)
    sleep(1)
    flicker(7)
    sleep(1)
    all_on()
    wait()

def chance():
    print('chance')
    all_on()
    wait()
    flicker(7)
    all_on()
    wait()
    all_off()
    sleep(2)
    for i in range(20):
        show_word('chance')
        sleep(0.25)
        all_off()
        sleep(0.25)
    all_on()
    wait()

def tdc():
    print('tdc')
    all_on()
    wait()
    for i in range(5):
        show_word('tour')
        sleep(1)
        show_word('de')
        sleep(1)
        show_word('chance')
        sleep(1)
    all_on()
    wait()

def race():
    print('race')
    all_on()
    wait()
    flicker(7)
    for i in range(20):
        all_off()
        sleep(0.5)
        show_word('race')
        sleep(0.5)
    flicker()
    flicker()
    all_off()
    sleep(0.5)
    flicker(6)
    all_on()
    wait()

def dance():
    print('dance')
    all_on()
    wait()
    flicker(7)
    all_off()
    sleep(0.5)
    show_word('dance')
    sleep(1)
    flicker(8, 'dance')
    show_word('dance')
    sleep(8)
    flicker(8, 'dance')
    show_word('dance')
    sleep(10)
    flicker(8, 'dance')
    show_word('dance')
    all_off()
    sleep(0.5)
    flicker(6)
    all_on()
    wait()

def touche():
    print('touche')
    all_on()
    wait()
    flicker(7)
    all_off()
    sleep(0.5)
    show_word('touche')
    sleep(1)
    flicker(8, 'touche')
    show_word('touche')
    sleep(8)
    all_off()
    sleep(0.5)
    flicker(6)
    all_on()
    wait()

def ouch():
    print('ouch')
    all_on()
    wait()
    flicker(7)
    for i in range(8):
        all_off()
        sleep(0.2)
        show_word('ouch')
        sleep(0.1)
    flicker(7)
    all_on()
    wait()

def detour():
    print('detour')
    all_on()
    wait()
    flicker(7)
    for i in range(4):
        all_off()
        sleep(0.5)
        show_word('de')
        sleep(0.8)
        show_word('tour')
        sleep(0.9)
        all_off()
        sleep(0.5)
    flicker(7)
    all_on()
    wait()

        
def chacha():
    print('chacha')
    all_on()
    wait()
    flicker(7)
    all_off()
    for i in range(4):
        sleep(0.5)
        show_word("dance")
        sleep(0.5)
        all_off()
        sleep(0.1)
        show_word("the")
        sleep(0.5)
        all_off()
        sleep(0.1)
        show_word("cha")
        sleep(0.2)
        all_off()
        sleep(0.2)
        show_word("cha")
        sleep(0.2)
        all_off()
        sleep(0.2)
        show_word("cha")
        sleep(0.5)
        all_off()
        sleep(0.5)
    flicker(7)
    all_on()
    wait()

def eh():
    print('eh')
    all_on()
    wait()
    flicker(7)
    show_word('eh')
    sleep(3)
    flicker(7)
    all_on()
    wait()


def inorder():
    print('inorder')
    all_on()
    wait()
    flicker(7)
    sleep(0.5)
    
    # for i in list("tourdechanCE"):
    #     show_word(i)
    #     sleep(1)
    # all_on
    # sleep(1)
    for i in range(4):
        for i in list("tourdechanCE"):
            show_word(i)
            sleep(0.7)
            if randint(0,12)==1:
                print("!")
                flicker(5,i)
                sleep(0.2)
                show_word(i)
                sleep(0.2)
                flicker(5,i)
                sleep(0.2)
                show_word(i)
                sleep(1.3)
        all_on()
        sleep(3)
    wait()



# ha()
# turd()
# tdc()
# race()
# dance()
# chance()
# turd()
# touche()
# detour()
# ouch()
# eh()
# chacha()
# inorder()
while True:
    choice([ha,turd,chance,dance,tdc,race,turd,touche,detour,ouch,eh,chacha,inorder])()