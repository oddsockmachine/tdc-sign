import opc, time
from random import randint
from utils import fade, interpolate

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')
numLEDs = 1024



def new_pastel(chance=0, pos=None):
    if randint(0,100) <= int(chance):
        return(0,0,0)
    if not pos:
        pos = randint(0,255)
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos*3)
        g = int(255 - pos*2)
        b = 255
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*2)
        g = 255
        b = int(pos*3)
    else:
        pos -= 170
        r = 255
        g = int(pos*3)
        b = int(255 - pos*2)
    if chance < 5:
        r = int(255 - ((255-r)*chance/2))
        g = int(255 - ((255-g)*chance/2))
        b = int(255 - ((255-b)*chance/2))
        #         r = min(255, int(r*chance))
        # g = min(255, int(g*chance))
        # b = min(255, int(b*chance))

    return (r, g, b)

flames = [fade((0,0,0),(0,0,0),1,1) for x in range(numLEDs)]


pixels = [ (0,0,0) ] * numLEDs
chance = 100.0
while True:
    chance -= 0.03
    if chance <= 0:
        chance = 100.0
    for i, f in enumerate(flames):
        if f.remaining == 0:
            f.last = f.next
            f.next = new_pastel(chance=chance)
            f.steps = max(1,randint(int(4*chance/10),int(30*chance/10)))
            f.remaining = f.steps
        else:
            f.remaining -= 1
        r = 1-(f.remaining / f.steps)
        c = interpolate(f.last, f.next, r)
        pixels[i] = (c[1],c[0],c[2])
    client.put_pixels(pixels)
    # print(chance)
    time.sleep(0.02)