# Check GPIO(s) to determine if temple/man burn mode enabled
# If either, override $1 50% of the time
LOGFILE="/opt/fadecandy/examples/python/log.txt"
rm $LOGFILE
date >> $LOGFILE
# By default, program is whatever is passed as arg 1
PROGRAM=$1
echo $PROGRAM >> $LOGFILE
# Check gpio 3, to see if it is pulled low by switch
if raspi-gpio get 23 | grep -q "level=0"; then
    echo "GPIO 23" >> $LOGFILE
    # If so, pick random number between 1-10
    RANDOM=$(bash -c 'echo $RANDOM')
    CHOICE=$((1 + $RANDOM % 10))
    echo $CHOICE >> $LOGFILE
    # If greater than 5, override program with "man_burn"
    if [ "$CHOICE" -gt "5" ]; then
        PROGRAM="man_burn"
        echo $PROGRAM >> $LOGFILE
    fi
fi

# Same for gpio 24 and temple_burn
if raspi-gpio get 24 | grep -q "level=0"; then
    echo "GPIO 24" >> $LOGFILE
    RANDOM=$(bash -c 'echo $RANDOM')
    CHOICE=$((1 + $RANDOM % 10))
    echo $CHOICE >> $LOGFILE
    if [ "$CHOICE" -gt "5" ]; then
        PROGRAM="temple_burn"
        echo $PROGRAM >> $LOGFILE
    fi
fi

# Copy the desired program into the "Running" file
cp /opt/fadecandy/examples/python/$PROGRAM.py /opt/fadecandy/examples/python/RUNNING.py
echo "Moved $PROGRAM to running" >> $LOGFILE
ls -l /opt/fadecandy/examples/python/RUNNING.py >> $LOGFILE
# Restart the service, which references the "running" file
sudo systemctl restart sign-leds.service
echo "restarted service" >> $LOGFILE
# Write the name into this text file, for debugging
echo $PROGRAM > /opt/fadecandy/examples/python/current.txt