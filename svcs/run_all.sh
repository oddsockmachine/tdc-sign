
while true
do
    trap 'echo caught interrupt and exiting;exit' INT
    date
    timeout --foreground 7s python chase.py
    timeout --foreground 7s python france_1.py
    timeout --foreground 7s python theme.py
    timeout --foreground 7s python france_2.py
    timeout --foreground 7s python squidsoup.py
    timeout --foreground 7s python sparkle.py
    timeout --foreground 7s python unicorn_puke.py
    timeout --foreground 7s python neon.py
    date
done