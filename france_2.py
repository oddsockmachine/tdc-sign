# Sparkling French flag 

import opc, time
from random import randint, choice

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')
# client.set_interpolation(False)
numLEDs=1024
leds_per_letter = 50


x = 255
blue = (0,0,x)
white = (x,x,x)
red = (0,x,0)


while True:
    pixels = [ (0,0,0) ] * numLEDs
    for i in range(leds_per_letter*4):
        pixels[i] = blue
    for i in range(leds_per_letter*4, leds_per_letter*8):
        pixels[i] = white
    for i in range(leds_per_letter*8, leds_per_letter*12):
        pixels[i] = red
    for i in range(numLEDs):
        if randint(0,1000) <= 1:
            pixels[i] = choice([red,white,blue,white,white,white])
    client.put_pixels(pixels)
    time.sleep(0.02)
