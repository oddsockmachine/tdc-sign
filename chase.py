# Rainbow chaser

import opc, time
from random import randint

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')

numLEDs = 1024

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 200
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 200
        b = int(pos*3)
    else:
        pos -= 170
        r = 200
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)


reset_counter = 100
rainbow_pos = 0
pixels = [ (0,0,0) ] * numLEDs
while True:
    next_led = (0,0,0)
    if randint(0,40) == 1:
        next_led = wheel(rainbow_pos+randint(0,5))
        next_led = wheel(randint(0,255))
        rainbow_pos += 10
        rainbow_pos %= 255
        reset_counter -= 1
        if reset_counter == 0:
            reset_counter = 100
            pixels = [ (0,0,0) ] * numLEDs
        # print(next_led)
        # print(rainbow_pos)
    

        pixels[0] = next_led
        pixels[1] = next_led
        pixels[2] = next_led
        pixels[3] = next_led
    for i  in reversed(range(numLEDs)):
        pixels[i] = pixels[i-1]
    client.put_pixels(pixels)
    time.sleep(0.04)


