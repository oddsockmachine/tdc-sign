# French flag subdivisions

import opc, time
from time import sleep
from utils import fade, interpolate
from random import randint, choice

from os import environ
host = environ.get("opc", "localhost")
client = opc.Client(host+':7890')
# client.set_interpolation(False)
numLEDs=1024

x = 255
colors = {
    0: (x,0,0),
    1: (0,0,x),
    2: (x,x,x)
}

pixels = [ (0,0,0) ] * numLEDs
segments = [1,2,4,8,16,32,64]
while True:
    for j in segments + list(reversed(segments)):
        for i in range(numLEDs):
            pos = int((i/j) % 3)
            c = colors[pos]
            pixels[i] = (c[1],c[0],c[2])
        client.put_pixels(pixels)
        time.sleep(4)